/*
	Define your network settings here
*/

#ifndef COMPUTERDEFINES_H
#define COMPUTERDEFINES_H

static unsigned int macAddr[] = {0x2C,0xFD,0xA1,0x70,0xAF,0x0D}; //Mac address for computer to wake
static String ipAddr="192.168.2.106"; //ip address to ping
static String SSID = "Harsjotorp";
static String WIFIPASSWORD = "Har5j0t0rp";

#endif