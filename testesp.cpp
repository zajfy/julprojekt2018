/*
	Test program for response from esp8266
*/


#include "AltSoftSerial.h"
#include <Arduino.h>

#define sw_serial_rx_pin 4 //  Connect this pin to TX on the esp8266
#define sw_serial_tx_pin 6 //  Connect this pin to RX on the esp8266
AltSoftSerial esp8266(sw_serial_rx_pin, sw_serial_tx_pin);

const unsigned long period = 100;

const unsigned long period2 = 1000;

int main()
{
	unsigned long startMillis;
	unsigned long currentMillis;
	
	// pinMode(sw_serial_rx_pin, INPUT);  
	
	Serial.begin(9600);
	Serial.print("Starting esp8266\r\n");
	esp8266.begin(9600);
	startMillis = millis();
	currentMillis = millis();
	while(currentMillis - startMillis >= period)
	{
		currentMillis = millis();
	}
	
	char c;
	while(1)
	{
		Serial.print("Sending an AT command...\r\n");
		startMillis = millis();
		while(currentMillis <= startMillis + period)
		{
			currentMillis = millis();
		}
		esp8266.print("AT/r/n");
		startMillis = millis();
		currentMillis = millis();
		while(currentMillis <= startMillis + period2)
		{
			Serial.print("why not this\r\n");
			Serial.print((int)currentMillis);
			Serial.print(":");
			Serial.print((int)millis());
			Serial.print("\r\n");
			currentMillis = millis();
		}
		// Serial.print(esp8266.available());
		Serial.print("\r\n");
		while (esp8266.available() > 0)
		{
			Serial.print("Got reponse from ESP8266: \r\n");
			
			while(currentMillis <= startMillis + period2)
			{
				currentMillis = millis();
			}
				
			c = esp8266.read();
			Serial.print("Reading response\r\n");
			Serial.print(c);
		} 
	}
	Serial.print("Shouldnt be here\r\n");
}