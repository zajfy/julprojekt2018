
#include "run.h"


enum states{SEARCH, FOUND} state;
AltSoftSerial esp8266(sw_serial_rx_pin, sw_serial_tx_pin);
// SerialESP8266wifi wifi(swSerial, swSerial, esp8266_reset_pin, Serial);

void setup()
{
	state = SEARCH;
	Serial.begin(9600);
	swSerial.begin(9600);
	if(!esp8266)
	{
		Serial.print("esp8266 not started");
	}
	
	Serial.print("Setup\n");
	esp8266.write("AT\r\n");
	while(esp8266.available() == 0)
	{
		// delay(50);
		Serial.print("No response yet");
		// Wait for wifi connection
	}
	while(esp8266.available() > 0)
	{
		Serial.print(esp8266.read());
	}		
	
	esp8266.print("AT+RST\r\n");
	esp8266.print("AT+CWMODE=1\r\n");
	char connectionString[50];
	sprintf(connectionString, "AT+CWJAP=\"%d\",\"%d\"\r\n", SSID, WIFIPASSWORD);
	esp8266.print(connectionString);
	while(esp8266.available() == 0)
	{
		// Wait for wifi connection
	}

}

void loop()
{
	Serial.print("Loop\n");
	switch(state)
	{
		case SEARCH:
			if(pingIpAddress())
			{
				state = FOUND;
				sendMagicPacket(swSerial);
			}
			break;
			
		case FOUND:
			if(!pingIpAddress())
			{
				state = SEARCH;
			}
			break;
	}
	// _delay_ms(30000);
}

int pingIpAddress()
{
	char data[16];
	int currentPos = 0;
	char espInput;
	// https://nurdspace.nl/ESP8266#AT_Commands
	char connectionString[50];
	sprintf(connectionString, "AT+PING=\"%d\"\r\n", ipAddr);
	esp8266.print(connectionString);
	while(esp8266.available() == 0)
    {
		//Waiting for answer
	}
	while(esp8266.available() > 0)
	{
		espInput = esp8266.read();
		data[currentPos++] = espInput;
		if(espInput == '\n')
		{
			break;
		}
	}
	if(data == "OK") //This does not work right now, as the AT+PING command also returns the latency TODO search the returning bytes for the OK command
	{
		return 1;
	}
	return 0;
}