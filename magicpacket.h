
/*
	Funktion to create the magic packet to wake the computer
*/

#ifndef MAGICPACKET_H
#define MAGICPACKET_H

#include <stdio.h>
#include <string.h>
#include <SoftwareSerial.h>

void sendMagicPacket(SoftwareSerial swSerial);
void createMagicPacket(unsigned char packet[]);

#endif