CPU = atmega328p
CPUFLAGS = -DF_CPU=16000000 -mmcu=$(CPU)
OBJECTS = $(patsubst %.cpp,%.o,$(wildcard *.cpp))
CPPFILES = $(wildcard *.cpp)
COMPORT = COM3

AVRINCLUDE = C:/LoseAVR/avr/include
ARDUINOINCLUDE = C:/LoseAVR/arduino_cores/uno/include
ARDUINOLIB = C:/LoseAVR/arduino_cores/uno/lib/libarduino-core.a
AVRSTL = C:/avr-stl/include

LIBPATHS = -I$(AVRINCLUDE) -I$(ARDUINOINCLUDE) -I$(AVRSTL)

# UNAME := $(shell uname)
# ifeq ($(UNAME), Linux)
# REMOVE=rm
# else
# endif
REMOVE=del

help:
	@echo 'help details:'
	@echo 'hex: compile hex file'
	@echo 'flash: install hex file'
	@echo 'program: compile and install hex'
	
hex:
	avr-g++ -Os $(CPUFLAGS) -c $(CPPFILES) $(LIBPATHS) 
	avr-g++ $(CPUFLAGS) -o awol.elf $(OBJECTS) $(ARDUINOLIB)
	avr-objcopy -O ihex awol.elf awol.hex
	$(REMOVE) $(OBJECTS)
	$(REMOVE) awol.elf
	
flash:
	avrdude -v -p $(CPU) -c arduino -P $(COMPORT) -b 115200 -D -U flash:w:awol.hex	
	
test:
	avr-g++ -Os $(CPUFLAGS) -c testesp.cpp AltSoftSerial.cpp $(LIBPATHS) 
	avr-g++ $(CPUFLAGS) -o testesp.elf testesp.o AltSoftSerial.o $(ARDUINOLIB)
	avr-objcopy -O ihex testesp.elf testesp.hex
	avrdude -v -p $(CPU) -c arduino -P $(COMPORT) -b 115200 -D -U flash:w:testesp.hex	
	$(REMOVE) $(OBJECTS)
	$(REMOVE) testesp.elf

program: hex flash