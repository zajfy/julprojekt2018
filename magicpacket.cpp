
#include "magicpacket.h"
#include "computerdefines.h"

//stump
void sendMagicPacket(SoftwareSerial swSerial)
{
	//Open udp connection to the routers broadcast address
	swSerial.write("AT+CIPSTART=4, UDP, 192.168.2.255, 8080, 1112, 0");
	while(swSerial.available() == 0)
	{
		//Waiting for response
	}
	unsigned char packet[102];
	createMagicPacket(packet);
	//Send the 102 byte package
	swSerial.write("AT+CIPSEND=4, 102");
	swSerial.write(*packet);
}

//https://github.com/GramThanos/WakeOnLAN/blob/master/WakeOnLAN.c
void createMagicPacket(unsigned char packet[])
{
	int i;
	// Mac Address Variable
	unsigned char mac[6];

	// 6 x 0xFF on start of packet
	for(i = 0; i < 6; i++){
		packet[i] = 0xFF;
		mac[i] = macAddr[i];
	}
	// Rest of the packet is MAC address of the pc
	for(i = 1; i <= 16; i++){
		memcpy(&packet[i * 6], &mac, 6 * sizeof(unsigned char));
	}
}