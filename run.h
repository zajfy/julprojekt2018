/*
	Main program body
*/


#ifndef RUN_H
#define RUN_H

#include <avr/io.h>
#include <util/delay.h>
#include "AltSoftSerial.h"
#include <Arduino.h>
// #include "SerialESP8266wifi.h"
#include "magicpacket.h"
#include "computerdefines.h"

#define sw_serial_rx_pin 4 //  Connect this pin to TX on the esp8266
#define sw_serial_tx_pin 6 //  Connect this pin to RX on the esp8266
#define esp8266_reset_pin 5 // Connect this pin to CH_PD on the esp8266, not reset. (let reset be unconnected)


int pingIpAddress();
void setup();
void loop();


#endif